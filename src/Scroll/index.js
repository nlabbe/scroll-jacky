import React, { Component } from 'react';
import './index.css';

class Scroll extends Component {

    constructor() {
        super();

        this.state = {
            page: 0,
            direction: null
        };

        this.scrollRef = null;
    }

    componentDidMount() {
        if(this.scrollRef) {
            this.scrollRef.addEventListener('touchstart', (e) => this.onTouchStart(e));
            this.scrollRef.addEventListener('touchmove', (e) => this.onTouchMove(e));
            this.scrollRef.addEventListener('touchend', (e) => this.onTouchEnd(e));
        }
    }

    onTouchStart(e) {
        this.setState({position: e.touches[0].pageY});
    }

    onTouchMove(e) {
        const { position } = this.state;

        if(position < e.touches[0].pageY) {
            this.setState({
                direction: 'down'
            });
        }else if(position > e.touches[0].pageY) {
            this.setState({
                direction: 'up'
            });
        }
    }

    onTouchEnd() {
        const { direction, page } = this.state;

        if(direction === 'down' && page > 0) {
            this.setPage(page - 1);
        }else if(direction === 'up') {
            this.setPage(page + 1);
        }
    }

    setPage(page) {
        this.setState({page: page, direction: null});
        window.scrollTo(0, window.innerHeight * page);
    }
    
    render() {
        const { scroll } = this.props;
        const { page } = this.state;

        const childrens = this.props.children
          && this.props.children.map((child, i) => <div className='page'>{React.cloneElement(child, {})}</div>);
    
        return (
          <div
            className={`scroll`}
            style={{height: `${childrens.length*100}%`}}
            ref={node => this.scrollRef = node}>
            <div className="overflow-container"
                style={{position: 'fixed', height: '100%'}}>
                <div className="scroll-container"
                    style={{top: `-${page * 100}%` || '0'}}>
                    {childrens}
                </div>
            </div>
          </div>
        );
      }
    
}

export default Scroll;
