import React, { Component } from 'react';
import './App.css';

import Scroll from './Scroll';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Scroll>
          <div className="content">hello</div>
          <div className="content">hello 2</div>
          <div className="content">hello 3</div>
          <div className="content">hello 4</div>
          <div className="content">hello 5</div>
        </Scroll>
      </div>
    );
  }
}

export default App;
